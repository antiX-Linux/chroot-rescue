��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  '     !   /     Q  "   h  !   �     �  '   �  ,   �          5                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Pierluigi Mario <pierluigimariomail@gmail.com>, 2020
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Non si riesce a trovare il file host %s  E' previsto un parametro dopo %s Chiudi questi processi Una directory è ancora montata %s Si prega di premere %s per uscire Argomento non chiaro dopo %s Queste directory sono ancora montate %s  non si sa come gestire link simbolici su %s host %s trovato su %s il target %s punta su %s 