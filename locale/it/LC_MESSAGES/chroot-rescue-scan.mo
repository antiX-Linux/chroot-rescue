��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     e     j     �     �     �     �     �  )   �  	   �  +   �  &      '   G     o  1   t  -   �  &   �     �  
             )  .   I  .   x     �  5   �     �     	                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Pierluigi Mario <pierluigimariomail@gmail.com>, 2019
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Arch Non si trova il comando %s Data Dispositivo Dir Directory: %s  Dispositivo: %s Distro Parametri aggiuntivi a riga di comando %s Etichetta Nessun sistema Linux è stato trovato in %s E' stato trovato un solo sistema Linux Seleziona un sistema Linux da visionare Esci Riscansiona tutte le partizioni per sistemi Linux Ricerca tutte le partizioni per sistemi Linux Scansione delle directory in corso ... Ricerca partizioni ... Dimensione Avvio di %s Strano, %s non è una directory La directory superiore %s non è una directory La directory superiore %s non è stata trovata Parametro sconosciuto %s Usa il comando %s o %s per tornare al menu principale Si visiona la distro %s fatto 