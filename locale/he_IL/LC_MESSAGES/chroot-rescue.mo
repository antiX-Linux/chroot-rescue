��          t      �                 .     L  !   d     �     �  &   �  '   �     
         5  5   C  *   y  -   �  .   �  )     '   +  4   S  E   �      �     �                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2021
Language-Team: Hebrew (Israel) (https://www.transifex.com/anticapitalista/teams/10162/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 לא ניתן למצוא את קובץ המארח %s! אמור להיות משתנה אחרי %s כעת בחיסול התהליכים הללו תיקייה אחת עדיין מעוגנת %s נא ללחוץ על "%s" כדי לצאת הארגומנט שאחרי %s שגוי התיקיות האלו עדיין מעוגנות %s לא ידוע כיצד לטפל בקישורים סמליים אל %s המארח %s נמצא תחת %s היעד %s מצביע אל %s 