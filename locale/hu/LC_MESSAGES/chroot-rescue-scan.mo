��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �               9     @  
   H     S     n      }     �  )   �  $   �  5   �  	   *  9   4  3   n     �     �     �     �     �  '     )   ,     V  ^   o  "   �     �                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Feri, 2022
Language-Team: Hungarian (https://www.transifex.com/anticapitalista/teams/10162/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Arch Nem található a parancs: %s Dátum Eszköz Könyvtár Könyvtár: %s Eszköz: %s Disztribúció Extra parancssor paraméterek %s Címke Nem találhatók Linux rendszerek itt: %s Csak egy Linux rendszer található. Válassza ki a meglátogatni kívánt Linux rendszert Kilépés Összes partíció újra átnézése Linux rendszerekért Összes partíció átnézése Linux rendszerekért Könyvtárak átnézése... Partíciók átnézése... Méret %s indítása %s nem egy könyvtár Csatolási könyvtár nem egy mappa: %s Csatolási könyvtár nem található: %s Ismeretlen paraméter %s Használja az %s parancsot, vagy %s billentyűkombinációt a menühöz való visszatéréshez Disztribúció meglátogatása: %s kész 