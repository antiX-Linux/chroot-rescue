��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5     �      �     �  &   	  +   0     \  &   {  4   �     �     �                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Feri, 2022
Language-Team: Hungarian (https://www.transifex.com/anticapitalista/teams/10162/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Nem található %s fájl! Hiányzó paraméter ezután: %s Folyamatok kilövése Egyetlen mappa még csatolva maradt %s Nyomja meg az %s billentyűt a kilépéshez Gyanús paraméter ezután: %s Ezek a mappák még csatolva vannak %s nem sikerült a szimbolikus link kezelése ehhez: %s a %s fájl itt található: %s %s célpont ide mutat: %s  