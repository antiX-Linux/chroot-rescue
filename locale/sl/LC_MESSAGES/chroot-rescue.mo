��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  %        3     Q  )   j     �     �  +   �  6   �      .     O                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2023
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Nisem našel datoeteke gostitelja %s! Za %s se pričakuje parameter Eliminacija teh procesov En direktorij je še vedno priklopljen %s Prosimo, pritisnite %s za izhod Sumljiv argument za %s Ti direktoriji so še vedno priklopljeni %s ne vem, kako naj obravnavam simbolično povezavo na %s gostitelj %s je bil najden na %s cilj %s kaže na %s 