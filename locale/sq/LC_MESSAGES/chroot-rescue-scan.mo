��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     8     =     W     ]     e     i     �  (   �     �  #   �     �  )        0  ,   4  *   a     �     �     �     �     �  +   �      $     E  @   ^     �     �                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2022
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Arch S’u gjet dot urdhër %s Datë Pajisje Dre Drejtori: %s  Pajisje: %s Shpërndarje Parametra ekstra për rresht urdhrash %s Etiketë S’u gjetën sisteme Linux nën %s U gjet vetëm një sistem Linux Ju lutemi, përzgjidhni një sistem Linux Dil Rikontrollo krejt pjesët për sisteme Linux Kontrollo krejt pjesët për sisteme Linux Po kontrollohen drejtori … Po kontrollohen pjesë … Madhësi Po niset %s Çudi, %s s’është drejtori Drejtoria e epërme %s s’është drejtori S’u gjet drejtori e epërme %s Parametër %s i panjohur Përdorni urdhrin %s, ose %s që të ktheheni te menuja kryesore Po vizitohet shpërndarja %s u bë 