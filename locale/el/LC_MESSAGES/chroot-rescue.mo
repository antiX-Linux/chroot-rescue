��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  ;   �  7     9   P  M   �  "   �  -   �  V   )  E   �  %   �     �                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2019
Language-Team: Greek (https://www.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 το αρχείου υποδοχής %s δε βρέθηκε Αναμενόμενη παράμετρος μετά %s Θανάτωση αυτών των διαδικασιών Ένας κατάλογος είναι ακόμα συνδεδεμένος %s Πατήστε %sγια έξοδο Ύποπτο επιχείρημα μετά %s Αυτοί οι κατάλογοι είναι ακόμα τοποθετημένοι %s δεν ξέρω πώς να χειριστεί το symlink για %s το host %s βρέθηκε στο %s target %spoints to %s 