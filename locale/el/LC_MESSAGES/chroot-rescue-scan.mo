��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     :  >   ?     ~     �     �  )   �     �  H   �     +  3   <  3   p  D   �     �  e   �  S   ]  #   �  +   �     	     	  8   )	  O   b	  -   �	  &   �	  w   
  '   
     �
                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2019
Language-Team: Greek (https://www.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Arch Δεν ήταν δυνατή η εύρεση εντολής %s Ημερομηνία Συσκευή Κατ Κατάλογος: %s Συσκευή: %s Διανομή Επιπλέον παραμέτρους γραμμής εντολών %s Επιγραφή Δεν βρέθηκαν συστήματα Linux %s Μόνο ένα σύστημα Linux βρέθηκε Επιλέξτε ένα σύστημα Linux για επίσκεψη Έξοδος  Επανατοποθετήστε όλα τα διαμερίσματα για συστήματα Linux Σάρωση όλων των κατατμήσεων για συστήματα Linux Σάρωση καταλόγων ... Σάρωση διαμερισμάτων ... Μέγεθος το %s ξεκίνησε Παράξενη, %sδεν είναι κατάλογος Ο κορυφαίος κατάλογος %sδεν είναι κατάλογος Ο κατάλογος %sδεν βρέθηκε Άγνωστη παράμετρος %s Χρησιμοποιήστε την εντολή %sή %sγια να επιστρέψετε στο κύριο μενού Επίσκεψη τη διανομή %s Ολοκληρώθηκε 