��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  "   �  $        6  '   O     w      �  -   �  ,   �          $                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020
Language-Team: Galician (Spain) (https://www.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 Ficheiro hospedeiro%s non atopado! Esperado un parámetro despois de %s Aniquilando este proceso Aínda está conectado un directorio %s Premer %s para saír Argumento dubidoso despois de %s Estes directorios aínda están conectados %s utilización de symlink para %s descoñecida hospedeiro %s atopado en %s o destino %s apunta para %s 