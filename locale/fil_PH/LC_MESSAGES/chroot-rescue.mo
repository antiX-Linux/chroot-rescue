��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  !   *  ,   L     y  -   �     �  0   �  /     )   E     o     �                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: sonny nunag <sonnynunag@yahoo.com>, 2019
Language-Team: Filipino (Philippines) (https://www.transifex.com/anticapitalista/teams/10162/fil_PH/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fil_PH
Plural-Forms: nplurals=2; plural=(n == 1 || n==2 || n==3) || (n % 10 != 4 || n % 10 != 6 || n % 10 != 9);
 Di maarimg mahanap ang%s na file! Inaasahang may parameter sa pagtatapos ng %s Pinapatay ang mga prosesong ito Isang directory ay kasalukuyang naka mount %s Pindutin ang  %s para umalis May kahinahinalang argumento sa pagtatapos ng %s Ang mga directory ay kasalukuyang naka mount %s di alam kung paano hawakan ang symlink %s Ang host %s ay natagpuan sa %s Ang target %s ay tumuturo sa %s 