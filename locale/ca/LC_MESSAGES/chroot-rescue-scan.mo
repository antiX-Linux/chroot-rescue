��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     .     3     K     P     \     `     ~  .   �  	   �  #   �  !   �  .   
     9  7   >  4   v     �     �     �     �  #   �  ,     $   A     f  6        �     �                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2019
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Arch No he trobat l'ordre %s Data Dispositiu  Dir Directori: %s  Dispositiu: %s Distribució Paràmetres addicionals a la línia d'ordres%s Etiqueta  No he trobat sistemes Linux sota %s Només he trobat un sistema Linux Si us plau, trieu un sistema Linux per visitar Surt Re-escaneja totes les particions cercant sistemes Linux Escaneja totes les particions cercant sistemes Linux Escanejant directoris... Escanejant particions... Mida  Iniciant %s  És estrany, %s no és un directori El directori superior %s no és un directori No he trobat el directori superior%s Paràmetre desconegut %s Useu l'ordre %s o bé %s per tornar al menú principal Visitant la distribució%s fet 