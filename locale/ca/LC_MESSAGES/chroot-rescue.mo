��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  -   �  %   �     $  %   =     c  !   �  *   �  -   �     �                     	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2019
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 No he pogut trobar el fitxer d'amfitrió %s ! S'espera un paràmetre després de %s Matant aquests processos Encara hi ha un directori muntat a %s Si us plau premeu %s per sortir Argument sospitós després de %s Aquests directoris encara estan muntats %s no sé com gestionar l'enllaç simbòlic a %s amfitrió %s trobat a %s la destinació %s apunta a %s 