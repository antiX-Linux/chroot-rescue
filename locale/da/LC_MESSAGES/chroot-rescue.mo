��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  !   �     �     �  "     $   5     Z  &   v  /   �     �     �                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: scootergrisen, 2019
Language-Team: Danish (https://www.transifex.com/anticapitalista/teams/10162/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
 Kunne ikke finde værtens %s-fil! Ventede en parameter efter %s Dræber processerne Der er stadig monteret en mappe %s Tryk venligst på %s for at afslutte Mistroisk argument efter %s Følgende mapper er stadig monteret %s ved ikke hvordan symlink til %s skal håndteres værten %s blev fundet på %s målet %s peger til %s 