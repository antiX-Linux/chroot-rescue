��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5     �     �          6     V     o  $   �  ,   �     �     �                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2020
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Fant ikke filen %s hos verten. Forventet en parameter etter %s Avslutter disse prosessene En mappe er fortsatt montert %s Trykk %s for å avslutte Mistenkelig parameter etter %s Disse mappene er fortsatt montert %s vet ikke hvordan symlenker til %s håndteres fant vertens %s i %s målets %s peker til %s 