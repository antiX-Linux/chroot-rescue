��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  "   �  "   �       )   ;     e  %   {  .   �  2   �           $                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Oi Suomi On! <oisuomion@protonmail.com>, 2020
Language-Team: Finnish (https://www.transifex.com/anticapitalista/teams/10162/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Isäntätiedostoa %s ei löydetty! Odotettiin parametriä %s jälkeen Näiden prosessien sammutus Yksi hakemisto on edelleen liitettynä %s Paina %s poistuaksesi Epäilyttävä argumentti %s jälkeen Nämä hakemistot ovat edelleen liitettynä %s ei tietoa kuinka käsitellä symlink kohteeseen %s isäntä %s löytyi kohteessa %s kohde %s osoittaa kohteeseen %s 