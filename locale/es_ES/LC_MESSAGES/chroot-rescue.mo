��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  "   �  (        9  '   P     x  #   �  -   �  0   �          2                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Casper, 2021
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 ¡No se encontró archivo %s host! Se esperaba un parámetro después de %s Matando estos procesos Un directorio todavía está montado %s Por favor pulse %s para salir Argumento sospechoso después de %s Estos directorios todavía están montados %s no sé cómo gestionar el enlace simbólico a %s host %s encontrado en %s objetivo %s apunta a %s 