��          �   %   �      `     a     f     �     �     �     �     �      �     �  $   �     �  %        C  '   H  %   p     �     �     �     �     �  #   �          6  /   K     {     �  �  �     f     s     �     �     �      �     �  -   �     	  .     #   @  1   d     �  8   �  /   �          !     <     D  #   Q  -   u  &   �     �  5   �     	  
   9	                                       
            	                                                                         Arch Could not find command %s Date Device Dir Directory: %s  Device: %s Distro Extra command line parameters %s Label No Linux systems were found under %s Only one Linux system was found Please select a Linux system to visit Quit Rescan all partitions for Linux systems Scan all partitions for Linux systems Scanning directories ... Scanning partitions ... Size Starting %s Strange, %s is not a directory Top directory %s is not a directory Top directory %s not found Unknown parameter %s Use the %s command or %s to return to main menu Visiting distro %s done Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2019
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Arquitectura Comando %s não encontrado Data Dispositivo Directório Directório: %s  Dispositivo: %s Distribuição Parâmetros de linha de comando adicionais %s Rótulo Não foi encontrado nenhum sistema Linux em %s Só foi encontrado um sistema Linux Por favor selecione um sistema Linux para visitar Sair Voltar a procurar sistemas Linux em todas as partições Procurar sistemas Linux em todas as partições A pesquisar directórios ... A examinar partições ... Tamanho A iniciar %s Estranho, %s não é um directório Directório de topo %s não é um directório Directório de topo %s não encontrado Parâmetro %s desconhecido Usar o comando %s ou %s para voltar ao menu principal A visitar a distribuição %s concluído 