��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  8   �  3   �  "   '  ;   J  )   �  3   �  9   �  _     "   ~      �                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Tymofii Lytvynenko <till.svit@gmail.com>, 2022
Language-Team: Ukrainian (https://www.transifex.com/anticapitalista/teams/10162/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 Не вдалося знайти файл хоста %s! Очікується параметр після %s Знищити ці процеси Один каталог все ще змонтовано %s Натисніть %s, щоб вийти  Підозрілий аргумент після %s Ці каталоги все ще змонтовано %s невідомо, як працювати з символічним посиланням на%s хост %s знайдено в %s ціль %s вказує на %s 