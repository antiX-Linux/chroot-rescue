��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5          3     O  &   h  $   �     �  *   �  1   �     ,     B                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: joyinko <joyinko@azet.sk>, 2023
Language-Team: Czech (https://www.transifex.com/anticapitalista/teams/10162/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 Soubor host %s nenalezen! Očekávaný parametr po %s Zabíjí se tyto procesy Jeden adresář je stále připojen %s Prosím stiskněte %s pro ukončení Podezřelý argument po %s Tyto adresáře jsou stále připojené %s nevím jak pracovat se symbolickým odkazem na %s host %s nalezen na %s cíl %s poukazuje na %s 