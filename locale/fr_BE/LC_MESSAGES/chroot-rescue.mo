��          t      �                 .     L  !   d     �     �  &   �  '   �     
       �  5  *     &   ,     S  #   q     �     �  '   �  6   �     +     B                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Impossible de trouver le fichier hôte %s! Un paramètre était attendu après %s Arrêt des processus en cours Un répertoire est encore monté %s Appuyez sur %s pour quitter Argument suspect après %s Ces répertoires sont encore montés %s ne sait pas comment diriger le lien symbolique vers %s hôte %s trouvé à %s la cible %s pointe vers %s 