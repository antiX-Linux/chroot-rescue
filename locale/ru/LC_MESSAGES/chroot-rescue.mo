��          t      �                 .     L  !   d     �     �  &   �  '   �     
         5  N   K  3   �  M   �  C     ;   `  ;   �  E   �  ^     3   }  &   �                	          
                                  Could not find host %s file! Expected a parameter after %s Killing these processes One directory is still mounted %s Please press %s to exit Suspicious argument after %s These directories are still mounted %s do not know how to handle symlink to %s host %s found at %s target %s points to %s Project-Id-Version: antiX Development
Report-Msgid-Bugs-To: translation@antixlinux.org
PO-Revision-Date: 2019-05-06 23:34+0000
Last-Translator: Andrei Stepanov, 2022
Language-Team: Russian (https://www.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Не удалось найти файл %s на целевой системе! После %s оджидается параметр Принудительное завершение этих процессов Один каталог всё ещё примонтирован %s Пожалуйста, нажмите %s для выхода Подозрительный аргумент после %s Эти каталоги всё ещё примонтированы %s не знаю, как обрабатывать символическую ссылку на %s %s целевой системы найден в %s цель %s указывает на %s 