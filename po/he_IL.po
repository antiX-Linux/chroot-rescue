# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR antiX Linux
# This file is distributed under the same license as the antiX Development package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# עומר א״ש <omeritzicschwartz@gmail.com>, 2020
# Yaron Shahrabani <sh.yaron@gmail.com>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: antiX Development\n"
"Report-Msgid-Bugs-To: translation@antixlinux.org\n"
"POT-Creation-Date: 2019-06-11 02:43-0600\n"
"PO-Revision-Date: 2019-05-06 23:34+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2021\n"
"Language-Team: Hebrew (Israel) (https://www.transifex.com/anticapitalista/teams/10162/he_IL/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: he_IL\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;\n"

msgid "Could not find host %s file!"
msgstr "לא ניתן למצוא את קובץ המארח %s!"

msgid "host %s found at %s"
msgstr "המארח %s נמצא תחת %s"

msgid "target %s points to %s"
msgstr "היעד %s מצביע אל %s"

msgid "do not know how to handle symlink to %s"
msgstr "לא ידוע כיצד לטפל בקישורים סמליים אל %s"

msgid "Killing these processes"
msgstr "כעת בחיסול התהליכים הללו"

msgid "One directory is still mounted %s"
msgstr "תיקייה אחת עדיין מעוגנת %s"

msgid "These directories are still mounted %s"
msgstr "התיקיות האלו עדיין מעוגנות %s"

msgid "Expected a parameter after %s"
msgstr "אמור להיות משתנה אחרי %s"

msgid "Suspicious argument after %s"
msgstr "הארגומנט שאחרי %s שגוי"

msgid "Please press %s to exit"
msgstr "נא ללחוץ על \"%s\" כדי לצאת"
