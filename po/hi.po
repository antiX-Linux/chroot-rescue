# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR antiX Linux
# This file is distributed under the same license as the antiX Development package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Panwar108 <caspian7pena@gmail.com>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: antiX Development\n"
"Report-Msgid-Bugs-To: translation@antixlinux.org\n"
"POT-Creation-Date: 2019-06-11 02:43-0600\n"
"PO-Revision-Date: 2019-05-06 23:34+0000\n"
"Last-Translator: Panwar108 <caspian7pena@gmail.com>, 2020\n"
"Language-Team: Hindi (https://www.transifex.com/anticapitalista/teams/10162/hi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hi\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "Could not find host %s file!"
msgstr "%s होस्ट फाइल प्राप्ति विफल!"

msgid "host %s found at %s"
msgstr "%s होस्ट %s पर प्राप्त"

msgid "target %s points to %s"
msgstr "%s लक्ष्य %s हेतु निर्दिष्ट है "

msgid "do not know how to handle symlink to %s"
msgstr "%s हेतु प्रतीकात्मक लिंक का प्रबंधन अज्ञात है"

msgid "Killing these processes"
msgstr "ये प्रक्रिया तुरंत बंद करें"

msgid "One directory is still mounted %s"
msgstr "एक डायरेक्टरी अभी भी माउंट है %s"

msgid "These directories are still mounted %s"
msgstr "ये डायरेक्टरी अभी भी माउंट है %s"

msgid "Expected a parameter after %s"
msgstr "%s के उपरांत मापदंड आवश्यक"

msgid "Suspicious argument after %s"
msgstr "%s के उपरांत संदेहजनक मानदंड"

msgid "Please press %s to exit"
msgstr "बंद करने हेतु %s दबाएँ"
